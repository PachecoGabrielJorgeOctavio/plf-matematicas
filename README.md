# Conjuntos, Aplicaciones y funciones 

```plantuml
@startmindmap
<style>
mindmapDiagram {
  Linecolor black
}
</style>

* Conjunto y elementos
** No tienen definición
***_ ya que 
**** Son conceptos primitivos

**_ dan paso a
*** La teoria de conjuntos 
****_ que incluye
***** Inclusion de conjuntos
******_ es una
******* Noción entre dos conjuntos
********_ dice que
********* Un conjunto pertenece a otro
**********_ si
*********** Todos los elementos del primero estan en el segundo

***** Operaciones basicas entre conjuntos
******_ como
*******_ Interseccion  
******** Elementos que pertenecen simultaneamente \na dos o mas conjuntos
*******_ Unión 
******** Elementos que pertenecen a almenos un conjunto 
*******_ Complmentación 
******** Elementos que no pertenecen \na un conjunto dado

***** Cardinal de un conjunto
******_ es el 
******* Numero de elementos que conforman un conjunto
******_ para el cual 
******* Se implementan formulas 
********_ como
********* El cardinal de la union de conjuntos 
**********_ que da lugar a
*********** La acotación de cardinales

******** Aplicaciones y funciones
*********_ son el
********** Concepto matematico 
***********_ de 
************ Transformación o cambio
*************_ que
************** Convierte los elementos de un conjunto en otro

********* Aplicacion
********** Convierte los elementos de un conjunto inicial
***********_ en 
************ Un unico elemento de un conjunto final o destino
********** Tipos
*********** Inyectiva
*********** Sobreyectiva
*********** Biyectiva

**********_ permite
*********** La composición de aplicaciones

**********_ es una
*********** Función
************_ cuando
************* Se realiza en un conjunto de numeros
************_ son
************* Faciles de manejar
************* Representados facilmente en un plano

******_ producen
******* Diferencia de conjuntos

****_ maneja 
***** Conjunto Universal
******_ es un
******* Conjunto de referencia
********_ en el que 
********* Ocurre todo lo relacionado \nal contexto dado (teoria)
***** Conjuto Vacio
******_ es una
******* Necesidad lógica
********_ un 
********* Conjunto sin elementos

****_ utiliza 
***** Metodos de demostracion
******_ para validar
******* Las propiedades de los conjuntos


** Tienen una relación de pertenencia

**_ son
*** Representados Graficamente
****_ usando
***** Diagramas de Venn
@endmindmap
```
[Recurso](https://canal.uned.es/video/5a6f1b77b1111f15098b4609)

#Funciones  

```plantuml
@startmindmap
<style>
mindmapDiagram {
  Linecolor black
}
</style>

* Funciones
**_ es una
*** Aplicación especial  
****_ que contiene
***** Conjuntos de Números reales
**_ es
*** Representado Gráficamente
****_ por 
***** Plano cartesiano
******_ que da lugar a
******* La Grafica de la función 

**_ pueden ser
*** Crecientes
****_ cuando  
***** La variable independiente aumenta
*** Decrecientes
****_ cuando
***** La variable independiente disminuye
*** Continuas
****_ cuando  
***** Los valores resultantes de puntos cercanos
******_ están 
******* Cerca de valores antiguos
*** Discontinuas
****_ cuando
***** Se rompe la continuidad de la función 

**_ maneja
*** Intervalos
****_ lo que genera
***** Máximo
******_ es el 
******* Punto más alto en un intervalo
***** Mínimo
******_ es el 
******* Punto más bajo en un intervalo

*** Limites
****_ es el 
***** Valor al que se aproximan
******_ los
******* Valores de una función
********_ cuando 
********* Se acerca al punto de interés

**_ tienen
*** Derivadas
****_ sirve para 
***** Resolver el problema de la aproximación
******_ en una
******* Función compleja
********_ mediante una
********* Función lineal
**********_ por ejemplo
*********** La tangente de una curva
@endmindmap
```
[Referencia](https://canal.uned.es/video/5a6f2d09b1111f21778b457f) 

# La matemática del computador 

```plantuml
@startmindmap
<style>
mindmapDiagram {
  Linecolor black
}
</style>

* La matemática del computador 
** Aritmética finita
***_ trabaja con 
**** Números finitos de decimales
***_ tiene en cuenta
**** Aspectos matemáticos
*****_ como
****** El número aproximado
****** Errores absoluto relativos
****** Dígitos significativos
****** Truncamiento de números
*******_ consiste en
******** Eliminar cifras a la derecha a \npartir de una dada
****** Redondeo de números 
*******_ también llamado
******** Truncamiento refinado
*********_ consiste en
********** Intentar que el error sea el mas pequeño posible

**_ maneja
*** Sistema Numéricos
**** Sistema binario
*****_ enlaza con
****** Lógica booleana 
****** Pertenencia y no pertenencia de conjuntos
*****_ construye
****** Estructuras complejas
*******_ como
******** Representaciones
********* Graficas
**********_ Videos
**********_ Imágenes
**********_ Etc.
********* Numéricas
**********_ Enteros con signo
**********_ Complemento 2
**********_ Con punto flotante
**********_ Con desbordamiento

*****_ maneja 
****** Operaciones 
******* Adición binaria

**** Sistema Octal
**** Sistema Hexadecimal 
*****_ usado para
****** Representación compacta de números 

*** Aritmética en punto flotante

@endmindmap
```
[Referencia 1](https://canal.uned.es/video/5a6f1b76b1111f15098b45fa)
[Referencia 2](https://canal.uned.es/video/5a6f1b81b1111f15098b4640)